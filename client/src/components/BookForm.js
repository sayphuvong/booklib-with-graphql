import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useQuery, useMutation } from '@apollo/client';
import { getAuthors, getBooks } from '../graphql-client/queries';
import { addSingleBook } from '../graphql-client/mutations';

const BookForm = () => {
  const [newBook, setNewBook] = React.useState({
    name: '',
    genre: '',
    authorId: '',
  });

  const { loading, error, data } = useQuery(getAuthors);
  const [addBook, dataMutation] = useMutation(addSingleBook);


  if (loading) {
    console.info('BookForm > Loading authors...');
  }
  if (error) {
    console.error(error.message);
    console.info('BookForm > Error load authors!');
  }

  const onInputChange = (e) => {
    setNewBook({
      ...newBook,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    addBook({
      variables: {
        name: newBook.name,
        genre: newBook.genre,
        authorId: newBook.authorId,
      },
      refetchQueries: [{ query: getBooks }],
    });

    // Reset form
    setNewBook({
      name: '',
      genre: '',
      authorId: '',
    });
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group>
        <Form.Control type="text" placeholder="Book Name" name="name" onChange={onInputChange} value={newBook.name} />
      </Form.Group>
      <Form.Group className="pt-3">
        <Form.Control type="text" placeholder="Book Genre" name="genre" onChange={onInputChange} value={newBook.genre} />
      </Form.Group>
      <Form.Group className="pt-3">
        <Form.Control as="select" name="authorId" onChange={onInputChange} value={newBook.authorId} >
          <option disabled value="">Select author</option>
          {data?.authors?.map(author => (
            <option key={author.id} value={author.id}>{author.name}</option>
          ))}
        </Form.Control>
      </Form.Group>
      <Button className="mt-3" style={{ float: 'right' }} variant="info" type="submit">
        Add Book
      </Button>
    </Form>
  );
};

export default BookForm;
