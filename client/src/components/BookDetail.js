import React from 'react';
import Card from 'react-bootstrap/Card';
import { getSingleBook } from '../graphql-client/queries';
import { useQuery } from '@apollo/client';

const BookDetail = ({ bookId }) => {
  const { loading, error, data } = useQuery(getSingleBook, {
    variables: {
      id: bookId
    },
  });

  if (loading) return <p>Loading book details...</p>;
  if (bookId !== null && error) return <p>Error loading book details!</p>;

  const book = !loading && !error ? data.book : null;

  return (
    <Card bg="info" text="white" className="shadow">
      <Card.Body>
        {book === null ? <Card.Text>Please select a book</Card.Text> : (
          <>
            <Card.Title>{book.name}</Card.Title>
            <Card.Subtitle>{book.genre}</Card.Subtitle>
            <p>{book.author.name}</p>
            <p>Age: {book.author.age}</p>
            <p>All books by this author</p>
            <ul style={{ marginTop: "2px"}}>
              {book.author.books.map(bItem => (
                <li key={bItem.id}>
                  {bItem.name}
                </li>
              ))}
            </ul>
          </>
        )}
        
      </Card.Body>
    </Card>
  )
}

export default BookDetail;
