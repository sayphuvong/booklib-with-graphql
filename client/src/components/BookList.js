import React from 'react';
import Card from 'react-bootstrap/Card';
import CardColumns from 'react-bootstrap/CardColumns';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useQuery } from '@apollo/client';
import { getBooks } from '../graphql-client/queries';
import BookDetail from './BookDetail';

const BookList = () => {
  const [bookSelected, setBookSelected] = React.useState(null);

  const { loading, error, data } = useQuery(getBooks);

  if (loading) return <p>Loading books...</p>;
  if (error) return <p>Error when loading book!</p>;

  return (
    <Row>
      <Col xs={8}>
        <CardColumns>
          <Row>
            {data?.books?.map(book => (
              <Col key={book.id} xs={4} className="my-2">
                <Card
                  border="info"
                  text="info"
                  className="text-center shadow"
                  onClick={setBookSelected.bind(this, book.id)}
                >
                  <Card.Body>{book.name}</Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
        </CardColumns>
      </Col>
      <Col xs={4}>
        <BookDetail bookId={bookSelected} />
      </Col>
    </Row>
  )
}

export default BookList;
