import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useMutation } from '@apollo/client';
import { getAuthors } from '../graphql-client/queries';
import { addSingleAuthor } from '../graphql-client/mutations';

const AuthorForm = () => {
  const [newAuthor, setNewAuthor] = React.useState({
    name: '',
    age: 0,
  });

  const [addBook, dataMutation] = useMutation(addSingleAuthor);

  const onInputChange = (e) => {
    setNewAuthor({
      ...newAuthor,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    addBook({
      variables: {
        name: newAuthor.name,
        age: parseInt(newAuthor.age),
      },
      refetchQueries: [{ query: getAuthors }],
    });

    // Reset form
    setNewAuthor({
      name: '',
      age: 0,
    });
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group>
        <Form.Control type="text" placeholder="Author Name" name="name" onChange={onInputChange} value={newAuthor.name} />
      </Form.Group>
      <Form.Group className="pt-3">
        <Form.Control type="number" placeholder="Author Age" name="age" onChange={onInputChange} value={newAuthor.age} />
      </Form.Group>
      <Button className="mt-3" style={{ float: 'right' }} variant="info" type="submit">
        Add Author
      </Button>
    </Form>
  );
};

export default AuthorForm;
