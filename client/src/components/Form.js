import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import BookForm from '../components/BookForm';
import AuthorForm from '../components/AuthorForm';

const MyFrom = () => {
  return (
    <Row>
      <Col>
        <BookForm />
      </Col>
      <Col>
        <AuthorForm />
      </Col>
    </Row>
  );
};

export default MyFrom;
